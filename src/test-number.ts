import {
	ValidationSchemaFactory,
	ValidationNumberShort,
	ValidationNumberGreater,
	ValidationNumberPositive
} from 'h2a-tov'
import { prettyPrint } from './pretty-print'

const numberSchemaBuilder = ValidationSchemaFactory.instance.getNumberSchemaBuilder()
const numberSchema = numberSchemaBuilder
	.addRule(new ValidationNumberShort({ 'rules.number.short': 'Số này không phải là kiểu short' }))  // custom error message
	.addRule(new ValidationNumberGreater(2001, { 'rules.number.greater': 'Số này bé hơn 2001' }))  // custom error message
	.addRule(new ValidationNumberPositive())  // default error message
	.build()

export function testNumberSchema() {
	const validationResult1 = numberSchema.validate(2147483647)
	const validationResult2 = numberSchema.validate(2000)
	const validationResult3 = numberSchema.validate(2001)
	const validationResult4 = numberSchema.validate(2002)
	const validationResult5 = numberSchema.validate(-2147483647)
	const validationResult6 = numberSchema.validate(-2000)
	const validationResult7 = numberSchema.validate(-2001)
	const validationResult8 = numberSchema.validate(-2002)

	prettyPrint(2147483647, validationResult1)
	console.log('\n\n')
	prettyPrint(2000, validationResult2)
	console.log('\n\n')
	prettyPrint(2001, validationResult3)
	console.log('\n\n')
	prettyPrint(2002, validationResult4)
	console.log('\n\n')
	prettyPrint(-2147483647, validationResult5)
	console.log('\n\n')
	prettyPrint(-2000, validationResult6)
	console.log('\n\n')
	prettyPrint(-2001, validationResult7)
	console.log('\n\n')
	prettyPrint(-2002, validationResult8)
}

export function testArraySchema() {
	const inputs = [2147483647, 2000, 2001, 2002, -2147483647, -2000, -2001, -2002];

	for (const input of inputs) {
		const result = numberSchema.validate(input)

		prettyPrint(input, result)
		console.log('\n\n')
	}
}

import {
	ValidationSchemaFactory,
	ValidationFlagNullable,
	ValidationFlagOptional,
	ValidationObjectCompact,
} from 'h2a-tov'
import { prettyPrint } from './pretty-print'
import { TObject, TNumberTransformable } from 'h2a-tov/dist/common/types'

const numberSchemaBuilder = ValidationSchemaFactory.instance.getNumberSchemaBuilder()
const numberSchema = numberSchemaBuilder
	.setFlag(new ValidationFlagNullable(true))
	.setFlag(new ValidationFlagOptional(true))
	.build()


type Input = {
	name: TNumberTransformable,
	[key: string]: TNumberTransformable,
};

const objectSchemaBuilder = ValidationSchemaFactory.instance.getObjectSchemaBuilder<Input, TObject>()
const objectSchema = objectSchemaBuilder
	.addItemSchema('name', numberSchema)
	.addRule(new ValidationObjectCompact({ allowNull: true }))
	.build()

export function testObjectSchema() {
	const inputs: Input[] = [
		{ name: 12 },
		{ name: '12' },
		{ name: 19, nullProperty: null },
		{ name: 19, nullProperty: null, undefinedProperty: undefined },
	];

	for (const input of inputs) {
		const result = objectSchema.validate(input)

		prettyPrint(input, result)
		console.log('\n\n')
	}
}


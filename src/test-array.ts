import {
	ValidationSchemaFactory,
	ValidationArrayValidItems,
	ValidationFlagNullable,
	ValidationFlagOptional,
	ValidationArrayStripUnknown,
} from 'h2a-tov'
import { prettyPrint } from './pretty-print'

const numberSchemaBuilder = ValidationSchemaFactory.instance.getNumberSchemaBuilder()
const numberSchema = numberSchemaBuilder
	.setFlag(new ValidationFlagNullable(true))
	.setFlag(new ValidationFlagOptional(true))
	.build()

const arraySchemaBuilder = ValidationSchemaFactory.instance.getArraySchemaBuilder()
const arraySchema = arraySchemaBuilder
	.setItemSchema(numberSchema)
	.addRule(new ValidationArrayValidItems([1, 2, 3]))
	.addRule(new ValidationArrayStripUnknown({ stripNull: false, stripUndefined: true }))
	.build()


export function testArraySchema() {
	const inputs = [
		[1, 2, 3, 4],
		[1, 2, 3],
		[1, 2, 3, undefined],
		[4, null],
		[null, undefined],
		[],
	];

	for (const input of inputs) {
		const result = arraySchema.validate(input)

		prettyPrint(input, result)
		console.log('\n\n')
	}
}


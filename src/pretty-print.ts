import { TFinalizedValidationResult } from 'h2a-tov/dist/common/types'
import { ValidationError } from 'h2a-tov/dist/errors'

export function prettyPrint(input: any, validationResult: TFinalizedValidationResult<ValidationError, any>) {
	console.log('>>> Input:', input)
	console.log('>>> Validated value:', validationResult.validatedValue)
	console.table(validationResult.errors)
}
